import os
from django.contrib.auth.models import User
import json
#import RPi.GPIO as GPIO
import time
from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .models import DispositivoTrabajador, Dispositivo, Trabajador, EstacionTrabajador, EstacionBloqueo

from django.http import JsonResponse
from datetime import datetime


@csrf_exempt
def registrarUsuarioDispositivo(request):
    idTrabajador = request.POST.get('idTrabajador', '')
    print(idTrabajador)
    if idTrabajador != '':
        trabajador = Trabajador.objects.get(pk = idTrabajador)
        print(trabajador)

    idDispositivo = request.POST.get('idDispositivo', '')
    if idDispositivo != '':
        dispositivo = Dispositivo.objects.get(pk = idDispositivo)
        print(dispositivo)           
    indiceTrabajador = request.POST.get('indiceTrabajador', '')

    rol = int(request.POST.get('rol',''))
    if rol == 1: 
        rol = True 
    else: 
        rol = False
    
    print(rol)
    if indiceTrabajador != '' and trabajador and dispositivo: 
        dispositivoTrabajador = DispositivoTrabajador(dispositivo = dispositivo, trabajador=trabajador, indice_trabajador = indiceTrabajador)
        dispositivoTrabajador.save()
        dispositivoTrabajador.trabajador.rol = rol
        dispositivoTrabajador.save()
        dispositivoTrabajador.trabajador.save()
        return HttpResponse('Usuario registrado.')
    else:
        return HttpResponse('No se pudo registrar Usuario.')

@csrf_exempt
def listarTrabajadorBloqueo(request):
    if request.method == 'GET':
        idDispositivo = request.GET.get('idDispositivo', '')
        dispositivo = Dispositivo.objects.get(pk=idDispositivo)
        estacionBloqueo = dispositivo.estacionBloqueo
        trab = Trabajador.objects.all()
        trabajadores_e = estacionBloqueo.estacion_trabajador.all()
        trabajadores_id = []
        trabajadores_nombre = []
        huellas = []
        trabajadores = []
        for t in trab:
            #trabajadores_id.append(t.trabajador.id)
            #trabajadores_nombre.append(t.trabajador.name)
            h = DispositivoTrabajador.objects.filter(trabajador = t, dispositivo = dispositivo).first()
            if not h:
                #huella = h.indice_trabajador
                #huellas.append(h.indice_trabajador)
            #else:
            #    huella = -1
                #huellas.append(-1)

                trabajadores.append({
                "id": t.id,
                "nombre":t.name,
                "rol":t.rol,
                "huella":-1
                })


        data = {
        'trabajadores_id': trabajadores_id,
        'trabajadores_nombre': trabajadores_nombre,
        'huellas': huellas
        }
        
    
        return JsonResponse(trabajadores,safe=False)
        # print(dump)
        # return JsonResponse(data)
    else:
        return HttpResponseNotFound("<h1>Error POST</h1>")

@csrf_exempt
def bloquearTrabajadorDispositivo(request):
    idDispositivo = request.POST.get('idDispositivo', '')
    estado = int(request.POST.get('estado',''))
    #Obtengo el dispositivo
    if idDispositivo != '':
        dispositivo = Dispositivo.objects.get(pk = idDispositivo)

    
    #Obtengo los responsables de la estacion
    estaciones = dispositivo.estacionBloqueo.estacion_trabajador.all()
    responsables = estaciones.filter(trabajador__rol = 1) 

    indice = request.POST.get('indice', '')

    #obtengo el trabajador
    if indice != '' and dispositivo:
        dispositivoTrabajador = DispositivoTrabajador.objects.filter(indice_trabajador = indice, dispositivo = dispositivo).first()

    estacionTrabajadorIns = EstacionTrabajador.objects.filter(trabajador= dispositivoTrabajador.trabajador,estacion_bloqueo = dispositivo.estacionBloqueo).first()
    #estacion_trabajador_todos = EstacionTrabajador.objects.filter(state = estacionTrabajadorIns.BLOQUEADO).count() > 0
    estacion_trabajador_todos = EstacionTrabajador.objects.filter(estacion_bloqueo = dispositivo.estacionBloqueo).all()


    if estacionTrabajadorIns:
        pass
    else:
        estacionTrabajadorIns = EstacionTrabajador(trabajador = dispositivoTrabajador.trabajador, estacion_bloqueo = dispositivo.estacionBloqueo)

        estacionTrabajadorIns.save()
    
    if (estado == estacionTrabajadorIns.state):
        if(estacionTrabajadorIns.state == EstacionTrabajador.BLOQUEADO):
            return HttpResponse('blo')
        elif(estacionTrabajadorIns.state == EstacionTrabajador.DESBLOQUEADO):
            return HttpResponse('des')


    if estacionTrabajadorIns.trabajador.rol is False:
        #Realiza el bloqueo cuando hay por lo menos un responsable bloqueado
        tmp = True
        for r in responsables:
            if (r.state == 3 or r.state == 1):
                tmp = False
            else:
                tmp = True
                break
        

        if (tmp is False):
            return HttpResponse('NO')

    else:
        #Realiza el debloqueo cuando todos los trabajadores hayan desbloqueado
        if estacionTrabajadorIns.state == EstacionTrabajador.BLOQUEADO: 
            des = True
            trabajadores = estaciones.filter(trabajador__rol = 0)
            for t in trabajadores:
                if(t.state == 2):
                    des = False
                    break
                else:
                    des = True
            if (des is False):
                return HttpResponse('NOP')


    if estacionTrabajadorIns:
        if estacionTrabajadorIns.state == 2:
            estacionTrabajadorIns.state = 3
            estacionTrabajadorIns.dateDesbloqueo = datetime.now()
        elif estacionTrabajadorIns.state == 3:
            estacionTrabajadorIns.state = 2
            estacionTrabajadorIns.dateBloqueo = datetime.now()
        else:
            estacionTrabajadorIns.state = 2

        estacionTrabajadorIns.save()



    estacion_trabajador_blo = EstacionTrabajador.objects.filter(state = EstacionTrabajador.BLOQUEADO, estacion_bloqueo = dispositivo.estacionBloqueo).count()
    estacion_trabajador_des = EstacionTrabajador.objects.filter(state = EstacionTrabajador.DESBLOQUEADO,estacion_bloqueo = dispositivo.estacionBloqueo).count()
    print("BLO",estacion_trabajador_blo)
    print("DES", estacion_trabajador_des)

    estacionBloqueo = EstacionBloqueo.objects.get(pk=dispositivo.estacionBloqueo.pk)

    if estacionBloqueo:
        if estacion_trabajador_blo > 0:
            estacionBloqueo.state = 2
        elif estacion_trabajador_blo == 0:
            estacionBloqueo.state = 3
        else:
            estacionBloqueo.state = 1
        estacionBloqueo.save()
    """
    ISMA TE MAMASTE
    NO BORRES LO DE ARRIBA QUE SI FUNCIONA 
    EL DIA VIERNES JUEVEDS 27 DE SEPTIEMBRE DEL 2018 
    TE EXPLICAMOS COMO FUNCIONA
    CUALQUIER DUDA LLAMAR A 955275633 
    """
    # contBloqueados = 0
    # contDesbloqueados = 0
    # contPendientes = 0
    # #print("ESTE",dispositivo.estacionBloqueo.pk)
    #
    #
    # for t in estacion_trabajador_todos:
    #     if t.state == 2:
    #         contBloqueados = contBloqueados + 1
    #     elif t.state == 3:
    #         contDesbloqueados = contDesbloqueados + 1
    #     else :#t.state == 1:
    #         contPendientes = contPendientes + 1
    #
    # if contBloqueados == estacion_trabajador_todos.count():
    #     estacionBloqueo.state = 2
    #     estacionBloqueo.save()
    # elif contDesbloqueados == estacion_trabajador_todos.count():
    #     estacionBloqueo.state = 3
    #     estacionBloqueo.save()
    # else :#contPendientes == estacion_trabajador_todos.count():
    #     estacionBloqueo.state = 1
    #     estacionBloqueo.save()
    
    return HttpResponse('Exito')

    # if indice and dispositivo: 
    #     dispositivoTrabajador = DispositivoTrabajador(dispositivo = dispositivo, indice = indice)
    #     dispositivoTrabajador.save()   
    #     return HttpResponse('Datos recibidos Exitosamente!')
    # else:
    #     return HttpResponse('Error al recibir datos.')


@csrf_exempt
def getEstadoEstacion(request):
    idDispositivo = request.GET.get('idDispositivo', '')
    dispositivo = Dispositivo.objects.get(pk = idDispositivo)
    estacion = dispositivo.estacionBloqueo

    print(estacion)


    data = {
    'estacion': estacion.state,
    }
    
    return JsonResponse(data,safe=False)


@csrf_exempt
def registrarResponsables(request):
    trabajador = request.POST.get('trabajador_id','')
    rol = request.POST.get('rol','')
    Trabajador.objects.filter(pk = trabajador).update(rol=rol)

    return HttpResponse("Exito")



@csrf_exempt
def limpiar(request):
    estaciones = EstacionBloqueo.objects.all()

    for es in estaciones:
        estTrab = es.estacion_trabajador.all()
        for et in estTrab:
            et.state = 1
            et.save()

        es.state = 3
        es.save()

    servo(6)
    print(request)
#    return HttpResponse('Ok')
    return HttpResponseRedirect('/admin/bloqueoApp/estacionbloqueo/')

def servo(pulso):
    pass
    # GPIO.setmode(GPIO.BOARD)
    # GPIO.setup(40,GPIO.OUT)
    # p = GPIO.PWM(40,50)

    # p.start(0)
    # p.ChangeDutyCycle(pulso)

    # time.sleep(2)
    # p.ChangeDutyCycle(0)
    # p.stop()
    # GPIO.cleanup()

@csrf_exempt
def getResponsables(request):
    idDispositivo = request.GET.get('idDispositivo', '')
    dispositivo = Dispositivo.objects.get(pk=idDispositivo)
    estacionBloqueo = dispositivo.estacionBloqueo
    trabajadores_e = estacionBloqueo.estacion_trabajador.all()
    trabajadores = []
    for t in trabajadores_e:
        if t.trabajador.rol:
            h = DispositivoTrabajador.objects.filter(trabajador = t.trabajador, dispositivo = dispositivo).first()
            if h:
                if(t.state != 1):
                    trabajadores.append({
                        "nombre":t.trabajador.name,
                        "estado":t.state,
                    })
    

    return JsonResponse(trabajadores,safe=False)

@csrf_exempt
def getTrabajadore(request):
    idDispositivo = request.GET.get('idDispositivo', '')
    dispositivo = Dispositivo.objects.get(pk=idDispositivo)
    estacionBloqueo = dispositivo.estacionBloqueo
    trabajadores_e = estacionBloqueo.estacion_trabajador.all()
    trabajadores = []
    for t in trabajadores_e:
        if not t.trabajador.rol:
            h = DispositivoTrabajador.objects.filter(trabajador = t.trabajador, dispositivo = dispositivo).first()
            if h:
                if(t.state != 1):
                    trabajadores.append({
                        "nombre":t.trabajador.name,
                        "estado":t.state,
                    })
    

    return JsonResponse(trabajadores,safe=False)