from django.contrib import admin

from .models import *

# Register your models here.
admin.site.register(Question)
admin.site.register(Choice)
admin.site.register(Dispositivo)
@admin.register(EstacionBloqueo)
class EstacionBloqueoAdmin(admin.ModelAdmin):
    change_list_template = 'bloqueoApp/limpiar.html'

#admin.site.register(EstacionBloqueo)
admin.site.register(Trabajador)
admin.site.register(Departamento)
admin.site.register(DispositivoTrabajador)
admin.site.register(EstacionTrabajador)
