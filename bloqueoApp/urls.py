from django.urls import path

from . import views, servicios

app_name = 'bloqueoApp'
urlpatterns = [
    #ejemplo para acceder vía url: /bloqueoApp
    path('', views.IndexView.as_view(), name='index'),
    # ejemplo para acceder vía url: /bloqueoApp/5/
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    # ejemplo para acceder vía url: /bloqueoApp/5/results/
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    # ejemplo para acceder vía url: /bloqueoApp/5/vote/
    path('<int:question_id>/vote/', views.vote, name='vote'),

    path('<int:pk>/detalle_estacion_bloqueo/', views.EstacionBloqueoDetailView.as_view(), name='detalle_estacion_bloqueo'),

    #path('<int:pk>/dispositivo/', views.DispositivoView.as_view(), name='dispositivo'),

    #path('<int:pk>/trabajador/', views.TrabajadorView.as_view(), name='trabajador'),
    path('trabajadores/', views.TrabajadorView.as_view(), name='trabajadores'),

    path('<int:trabajador_id>/trabajadorTarjeta/', views.TrabajadorTarjetaView, name='trabajadorTarjeta'),

    path('<int:trabajador_id>/<int:estacion_id>/trabajadorDetalle/', views.TrabajadorDetalleView, name='trabajadorDetalle'),

    path('<int:trabajador_id>/<int:estacion_id>/trabajadorRegistro/', views.TrabajadorRegistroView, name='trabajadorRegistro'),

    path('<int:pk>/departamento/', views.DepartamentoView.as_view(), name='departamento'),

    path('listado_trabajadores_dispositivo/', views.DispositivoView.as_view(), name='listado_trabajadores_dispositivo'),

    #path('listado_trabajadores_estacion/', views.EstacionBloqueoListView.as_view(), name='listado_trabajadores_estacion'),
    path('', views.IndexView.as_view(), name='listado_trabajadores_estacion'),

    path('trabajadores_sin_huella/', servicios.listarTrabajadorBloqueo, name='trabajadores_sin_huella'),
    path('registrar_usuario_dispositivo/', servicios.registrarUsuarioDispositivo, name='registrar_usuario_dispositivo'),
    path('bloquear_trabajador_dispositivo/', servicios.bloquearTrabajadorDispositivo, name='bloquear_trabajador_dispositivo'),
    path('estado_estacion/', servicios.getEstadoEstacion, name='estado_estacion'),
    path('registrar_responsable/', servicios.registrarResponsables, name='registrar_responsables'),
    path('limpiar/',servicios.limpiar,name='limpiar'),
    path('getResponsables/',servicios.getResponsables,name='get-responsables'),
    path('getTrabajadores/',servicios.getTrabajadore,name='get-trabajadores'),
]   
