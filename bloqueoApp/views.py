from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.template import RequestContext
from django.template.loader import get_template
from django.views import generic
from django.views.generic.base import View
from .models import Choice, Question, EstacionBloqueo, Dispositivo, Trabajador, Departamento, DispositivoTrabajador, EstacionTrabajador
from django.db.models import F, Count
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from datetime import datetime
# Create your views here.

# class IndexView(generic.ListView):
#     template_name = 'bloqueoApp/index.html'
#     context_object_name = 'latest_question_list'

#     def get_queryset(self):
#         """Return the last five published questions."""
#         return Question.objects.order_by('-pub_date')[:5]

class EstacionBloqueoDetailView(generic.DetailView):
    model = EstacionBloqueo
    template_name = 'bloqueoApp/detalle_estacion_bloqueo.html'

class IndexView(generic.ListView):
    model = EstacionBloqueo
    template_name = 'bloqueoApp/listado_trabajadores_dispositivo.html'
    context_object_name = 'dispositivo_list'

class DispositivoView(generic.ListView):
    model = Dispositivo
    #template_name = 'bloqueoApp/dispositivo.html'
    template_name = 'bloqueoApp/listado_trabajadores_dispositivo.html'
    context_object_name = 'dispositivo_list'


class TrabajadorView(generic.ListView):
    model = Trabajador
    template_name = ('bloqueoApp/trabajadores.html')

    context_object_name = 'trabajadores_list'
    paginate_by = 1


def TrabajadorTarjetaView(request, trabajador_id):
    trabajador = get_object_or_404(Trabajador, pk=trabajador_id)
    print(trabajador_id)
    return render (request,'bloqueoApp/trabajadorTarjeta.html', {'trabajador':trabajador})

def TrabajadorDetalleView(request, trabajador_id, estacion_id):
    trabajador = get_object_or_404(Trabajador, pk=trabajador_id)
    estacion = get_object_or_404(EstacionBloqueo, pk=estacion_id)
    responsables = estacion.estacion_trabajador.filter(trabajador__rol = 1) 
    print(trabajador)
    print(estacion)
    print(responsables)
    context = {
        'trabajador':trabajador,
        'responsables':responsables
    }
    return render(request, 'bloqueoApp/trabajadorDetalle.html', context)
    #return render (request,'bloqueoApp/trabajadorDetalle.html', {'trabajador':trabajador}, {'responsables':responsables})
    

def TrabajadorRegistroView(request, trabajador_id, estacion_id):
    trabajador = get_object_or_404(Trabajador, pk=trabajador_id)
    estacion = get_object_or_404(EstacionTrabajador, pk=estacion_id)
    #responsables = estacion.trabajador.filter(trabajador__rol = 1) 
    print(trabajador)
    print(estacion.dateDesbloqueo)
    context = {
        'trabajador':trabajador,
        'estacion':estacion
    }
    return render(request, 'bloqueoApp/trabajadorRegistro.html', context)


class DepartamentoView(generic.DetailView):
    model = Departamento
    template_name = 'bloqueoApp/departamento_detail.html'


class DispositivoTrabajadorView(generic.ListView):
    model = DispositivoTrabajador
    #template_name = 'bloqueoApp/listado_trabajadores_dispositivo.html'
    context_object_name = 'dispositivo_trabajador_list'


class DetailView(generic.DetailView):
    model = Question
    template_name = 'bloqueoApp/detail.html'


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'bloqueoApp/results.html'


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        print("CARLOS")    
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        print("PEDRO")    
        # Redisplay the question voting form.
        return render(request, 'bloqueoApp/detail.html', {
             'question': question,
             'error_message': "No ha seleccionado ninguna opción.",
        })
    else:
        #selected_choice = Question.objects.select_for_update().get(pk=question_id)
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('bloqueoApp:results', args=(question.id,)))

# def index(request):
#     latest_question_list = Question.objects.order_by('-pub_date')[:5]
#     template = loader.get_template('bloqueoApp/index.html')
#     context = { 
#                 'latest_question_list': latest_question_list 
#               }
#     return HttpResponse(template.render(context, request))

# def detail(request, question_id):
#     question = Question.objects.get(pk=question_id)
#     template = loader.get_template('bloqueoApp/detail.html')
#     return HttpResponse(template.render({'question': question}, request))

# def results(request, question_id):
#     response = "You're looking at the results of question %s."
#     return HttpResponse(response % question_id)

# def vote(request, question_id):
#     question = get_object_or_404(Question, pk=question_id)
#     try:
#         print("CARLOS")    
#         selected_choice = question.choice_set.get(pk=request.POST['choice'])
#     except (KeyError, Choice.DoesNotExist):
#         print("PEDRO")    
#         # Redisplay the question voting form.
#         return render(request, 'bloqueoApp/detail.html', {
#              'question': question,
#              'error_message': "No ha seleccionado ninguna opción.",
#         })
#     else:
#         #selected_choice = Question.objects.select_for_update().get(pk=question_id)
#         selected_choice.votes += 1
#         selected_choice.save()
#         # Always return an HttpResponseRedirect after successfully dealing
#         # with POST data. This prevents data from being posted twice if a
#         # user hits the Back button.
#         return HttpResponseRedirect(reverse('bloqueoApp:results', args=(question.id,)))

# def results(request, question_id):
#     question = get_object_or_404(Question, pk=question_id)
#     return render(request, 'bloqueoApp/results.html', {'question': question})