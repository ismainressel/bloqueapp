import datetime
from datetime import datetime

from django.db import models
from django.utils import timezone
from enum import Enum
from django import forms

# Create your models here.

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    
    def __str__(self):
        return self.question_text
    
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    def __str__(self):
        return self.choice_text

class Departamento(models.Model):
    name = models.CharField(max_length=200)
    
    def __str__(self):
        return self.name

class Trabajador(models.Model):
    name = models.CharField(max_length=200)
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
    celular = models.CharField(max_length=200, default="403645")
    codigo = models.CharField(max_length=200, default="123456")
    dni = models.CharField(max_length=200, default="36192398")
    emergencyPhone = models.CharField(max_length=200, default="462877")
    tiempo = models.TimeField('Hora de Ingreso')
    image = models.ImageField(upload_to='profile_image', blank=True)
    rol = models.BooleanField(default = False)
    def __str__(self):
        return self.name

class EstacionBloqueo(models.Model):
    name = models.CharField(max_length=200)
    # responsable = models.OneToOneField(
    #     Trabajador,
    #     on_delete=models.CASCADE,
    #     primary_key=True,
    # )
    #responsable = models.ForeignKey(Trabajador, on_delete=models.CASCADE)
    creation_date = models.DateTimeField('date created')
    PENDIENTE_BLOQUEO = 1
    BLOQUEADO = 2
    DESBLOQUEADO = 3
    STATES = (
        (PENDIENTE_BLOQUEO, 'Pendiente de Bloqueo'),
        (BLOQUEADO, 'Bloqueado'),
        (DESBLOQUEADO, 'Desbloqueado'),
    )
    state = models.IntegerField(choices=STATES, default=PENDIENTE_BLOQUEO)
    responsableState = models.IntegerField(choices=STATES, default=PENDIENTE_BLOQUEO)
    
    def __str__(self):
        return self.name

    def was_published_recently(self):
        return self.creation_date >= timezone.now() - datetime.timedelta(days=1)
    
    def __int__(self):
        return self.state

class Dispositivo(models.Model):
    estacionBloqueo = models.OneToOneField(
        EstacionBloqueo,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    name = models.CharField(max_length=200)
    
    def __str__(self):
        return self.name



class DispositivoTrabajador(models.Model):
    trabajador = models.ForeignKey(Trabajador, on_delete=models.CASCADE, related_name="dispositivo_trabajador")
    dispositivo= models.ForeignKey(Dispositivo, on_delete=models.CASCADE)
    name = models.CharField(max_length=200, default='Dispositivo Trabajador')
    indice_trabajador = models.IntegerField(default=0)
    class Meta:
        ordering = ["dispositivo"]
    

class EstacionTrabajador(models.Model):
    trabajador = models.ForeignKey(Trabajador, on_delete=models.CASCADE, related_name="estacion_trabajador")
    estacion_bloqueo = models.ForeignKey(EstacionBloqueo, on_delete=models.CASCADE, related_name="estacion_trabajador" )
    name = models.CharField(max_length=200, default='Dispositivo Trabajador')
    PENDIENTE_BLOQUEO = 1
    BLOQUEADO = 2
    DESBLOQUEADO = 3
    STATES = (
        (PENDIENTE_BLOQUEO, 'Pendiente de Bloqueo'),
        (BLOQUEADO, 'Bloqueado'),
        (DESBLOQUEADO, 'Desbloqueado'),
    )
    state = models.IntegerField(choices=STATES, default=PENDIENTE_BLOQUEO)
    dateBloqueo = models.DateTimeField('Fecha Bloqueo', default=datetime.now, blank=True)
    dateDesbloqueo = models.DateTimeField('Fecha Desbloqueo', default=datetime.now, blank=True)
    
    def was_published_recently(self):
        return self.creation_date >= timezone.now() - datetime.timedelta(days=1)
